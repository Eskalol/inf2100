package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;
/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for Factor
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class Factor extends PascalSyntax{
    Constant constant = null;
    Variable variable = null;
    FuncCall funcCall = null;
    InnerExpr innerExpr = null;
    Negation negation = null;
    Type type;

    /**
     * Konstruktoer for Factor
     *
     * @param lNum linje nummer
     */
    public Factor(int lNum){
        super(lNum);
    }


    /**
     * check function for Factor
     * @param curScope scope
     * @param lib library
     */
    public void check(Block curScope, Library lib){
        if(constant!=null){
            constant.check(curScope, lib);
            this.type = constant.type;
        }
        if(variable!=null) {
            variable.check(curScope, lib);
            this.type = variable.type;
        }
        if(funcCall!=null) {
            funcCall.check(curScope, lib);
            this.type = funcCall.type;
        }
        if(innerExpr!=null) {
            innerExpr.check(curScope, lib);
            this.type = innerExpr.type;
        }
        if(negation!=null) {
            negation.check(curScope, lib);
            this.type = negation.type;
        }
    }


    /**
     * genCode metode for Factor
     * @param f CodeFile
     * @param b Block
     */
    @Override
    public void genCode(CodeFile f, Block b){
        if(constant!=null) constant.genCode(f, b);
        if(variable!=null) variable.genCode(f, b);
        if(funcCall!=null) funcCall.genCode(f, b);
        if(innerExpr!=null) innerExpr.genCode(f, b);
        if(negation!=null) negation.genCode(f, b);
    }

    /**
     * identify metode for Factor
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<factor> on line " + lineNum;
    }

    /**
     * prettyPrint metode for Factor
     */
    public void prettyPrint(){
        if(constant != null) constant.prettyPrint();
        if(variable != null) variable.prettyPrint();
        if(funcCall != null) funcCall.prettyPrint();
        if(innerExpr != null) innerExpr.prettyPrint();
        if(negation != null) negation.prettyPrint();
    }

    /**
     * metoden parser en Factor
     *
     * @param s Scanneren
     * @return et Factor objekt
     */
    public static Factor parse(Scanner s) {
        enterParser("factor");

        Factor f = new Factor(s.curLineNum());
        switch(s.curToken.kind){
            case notToken: f.negation = Negation.parse(s); break;
            case leftParToken: f.innerExpr = InnerExpr.parse(s); break;
            case intValToken: f.constant = Constant.parse(s); break;
            case stringValToken: f.constant = Constant.parse(s); break;
            case nameToken:
                switch(s.nextToken.kind){
                    case leftParToken: f.funcCall = FuncCall.parse(s); break;
                    default: f.variable = Variable.parse(s); break;

                }
        }

        leaveParser("factor");
        return f;
    }
}
