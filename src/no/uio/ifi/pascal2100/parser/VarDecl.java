package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.CodeFile;
import no.uio.ifi.pascal2100.main.Main;
import no.uio.ifi.pascal2100.scanner.Scanner;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import java.util.ArrayList;

/**
 * Created by eska on 16.09.15.
 */

/**
 * klasse for VarDecl
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class VarDecl extends PascalDecl{

    /**
     * Konstruktoer for VarDecl
     *
     * @param lNum linje nummer
     * @param id navn til deklarasjon
     */
    public VarDecl(String id, int lNum) {
        super(id, lNum);
    }

    /**
     * check function for VarDecl
     * @param curScope scope
     * @param lib library
     */
    public void check(Block curScope, Library lib) {
        curScope.addDecl(this.name, this);
        type.check(curScope, lib);
        this.type = type.type;
    }

    /**
     * genCode for VarDecl metoden henter ut variabelen til eax.
     * @param f
     * @param b
     */
    @Override
    void genCode(CodeFile f, Block b) {
        f.genInstr("", "movl", (-4*this.declLevel) + "(%ebp),%edx ", "");
        f.genInstr("", "movl", -this.declOffset + "(%edx),%eax", this.name + "");
    }

    /**
     * identify metode for VarDecl
     *
     * @return returnere identify String
     */
    public String identify() {
        return "<var decl> on line " + lineNum;
    }

    /**
     * prettyPrint metode for VarDecl
     */
    public void prettyPrint() {
        Main.log.prettyPrint(name);
        Main.log.prettyPrint(" : ");
        type.prettyPrint();
        Main.log.prettyPrint(";");
    }

    /**
     * metoden parser en VarDecl
     *
     * @param s Scanneren
     * @return et VarDecl objekt
     */
    public static VarDecl parse(Scanner s) {
        enterParser("var decl");
        VarDecl vd = new VarDecl(s.curToken.id, s.curLineNum());
        s.skip(nameToken);
        s.skip(colonToken);
        vd.type = Type.parse(s);
        s.skip(semicolonToken);

        leaveParser("var decl");
        return vd;
    }
}
