package no.uio.ifi.pascal2100.parser;
import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import java.util.ArrayList;
/**
 * Created by eska on 16.09.15.
 */

/**
 * klasse for ParamDeclList
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class ParamDeclList extends PascalSyntax{
    ArrayList<ParamDecl> paramDecl;
    /**
     * Konstruktoer for ParamDeclList
     *
     * @param lNum linje nummer
     */
    public ParamDeclList(int lNum) {
        super(lNum);
        paramDecl = new ArrayList<ParamDecl>();
    }

    /**
     * genCode for ParamDeclList
     * @param f CodeFile
     * @param b Block
     */
    @Override
    public void genCode(CodeFile f, Block b) {
        int nextOffset = 8;
        for(ParamDecl pd: paramDecl) {
            pd.declLevel = b.getBlocklvl();
            pd.declOffset = nextOffset;
            nextOffset += 4;
        }
    }

    /**
     * check meteode for ParamDeclList
     *
     * @param curScope scope
     * @param lib library
     */
    public void check(Block curScope, Library lib) {
        for(ParamDecl pd: paramDecl)
            pd.check(curScope, lib);
    }

    /**
     * identify metode for ParamDeclList
     *
     * @return returnere identify String
     */
    public String identify() { return "<param decl list> on line " + lineNum; }

    /**
     * prettyPrint metode for ParamDeclList
     */
    public void prettyPrint() {
        Main.log.prettyPrint("(");
        for(int i = 0; i < paramDecl.size(); i++) {
            paramDecl.get(i).prettyPrint();
            if(i < paramDecl.size() -1) Main.log.prettyPrint("; ");
        }
        Main.log.prettyPrint(")");
    }

    /**
     * metoden parser en ParamDeclList
     *
     * @param s Scanneren
     * @return et ParamDeclList objekt
     */
    public static ParamDeclList parse(Scanner s) {
        enterParser("param decl list");
        ParamDeclList pdl = new ParamDeclList(s.curLineNum());
        s.skip(leftParToken);
        while(s.curToken.kind == nameToken){
            pdl.paramDecl.add(ParamDecl.parse(s));
            if(s.curToken.kind == rightParToken) break;
            s.skip(semicolonToken);
        }
        s.skip(rightParToken);
        leaveParser("param decl list");
        return pdl;
    }
}
