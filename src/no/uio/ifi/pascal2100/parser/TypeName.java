package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;
/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for TypeName
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class TypeName extends Type{
    String name;

    /**
     * Konstruktoer for TypeName
     *
     * @param lNum linje nummer
     * @param id navn til deklarasjon
     */
    public TypeName(String id, int lNum){
        super(lNum);
        name = id;
    }

    /**
     * funksjonen sjekker om typeNavnet finnes i scope sine decls
     * @param curScope scope
     * @param lib library
     */
    @Override
    public void check(Block curScope, Library lib) {
        this.type = curScope.findDecl(this.name, this).type;
    }

    @Override
    void genCode(CodeFile f, Block b) {

    }

    /**
     * identify metode for TypeName
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<type name> on line " + lineNum;
    }

    /**
     * prettyPrint metode for TypeName
     */
    public void prettyPrint(){
        Main.log.prettyPrint(name);
    }

    /**
     * metoden parser en TypeName
     *
     * @param s Scanneren
     * @return et TypeName objekt
     */
    public static TypeName parse(Scanner s) {
        enterParser("type name");

        TypeName tn = new TypeName(s.curToken.id, s.curLineNum());
        s.skip(nameToken);

        leaveParser("type name");
        return tn;
    }
}