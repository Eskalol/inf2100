package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;
/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for NumericLiteral
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class NumericLiteral extends PascalSyntax{
   int number;

    /**
     * Konstruktoer for NumericLiteral
     *
     * @param lNum linje nummer
     */
    public NumericLiteral(int lNum){
        super(lNum);
    }

    @Override
    void check(Block curScope, Library lib) {

    }

    /**
     * genCode for NumericLLiteral
     * @param f CodeFile
     * @param b Block
     */
    @Override
    public void genCode(CodeFile f, Block b) {
        f.genInstr("", "movl", "$" + number + ",%eax", number + " to %eax");
    }

    /**
     * identify metode for NumericLiteral
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<numeric literal> on line " + lineNum;
    }

    /**
     * prettyPrint metode for NumericLiteral
     */
    public void prettyPrint(){
        Main.log.prettyPrint("" + number);
    }

    /**
     * metoden parser en NumericLiteral
     *
     * @param s Scanneren
     * @return et NumericLiteral objekt
     */
    public static NumericLiteral parse(Scanner s) {
        enterParser("numeric literal");

        NumericLiteral nl = new NumericLiteral(s.curLineNum());
        nl.number = s.curToken.intVal;
        s.skip(intValToken);

        leaveParser("numeric literal");
        return nl;
    }
}