package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;

import javax.lang.model.element.TypeElement;
/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for Variable
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class Variable extends PascalDecl{
    Expression expr = null;
    Type type;
    /**
     * Konstruktoer for Variable
     *
     * @param lNum linje nummer
     * @param id navn til variabel
     */
    public Variable(String id, int lNum){
        super(id, lNum);
    }

    /**
     * metoden finner dekllarasjonen og kaller paa deklarasjonens genCode.
     * @param f CodeFile
     * @param b variabelens block
     */
    @Override
    public void genCode(CodeFile f, Block b) {
        PascalDecl pd = b.findDecl(this.name);
        pd.genCode(f, b);
    }

    /**
     *
     * @param curScope scope
     * @param lib library
     */
    public void check(Block curScope, Library lib) {
        this.type = curScope.findDecl(this.name, this).type;
        if(expr != null) expr.check(curScope, lib);
    }

    /**
     * identify metode for Variable
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<variable> on line " + lineNum;
        }

    /**
     * prettyPrint metode for Variable
     */
    public void prettyPrint(){
        Main.log.prettyPrint(name);
        if(expr != null){
            Main.log.prettyPrint("[");
            expr.prettyPrint();
            Main.log.prettyPrint("]");
        }
    }

    /**
     * metoden parser en Variable
     *
     * @param s Scanneren
     * @return et Variable objekt
     */
    public static Variable parse(Scanner s) {
        enterParser("variable");

        Variable v = new Variable(s.curToken.id, s.curLineNum());
        s.skip(nameToken);
        if(s.curToken.kind == leftBracketToken){
            s.skip(leftBracketToken);
            v.expr = Expression.parse(s);
            s.skip(rightBracketToken);
        }
        leaveParser("variable");
        return v;
    }
}