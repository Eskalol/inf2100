package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;

import java.util.ArrayList;

/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for RelOpr
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class RelOpr extends PascalSyntax{
    Boolean equal = false;
    Boolean less = false;
    Boolean lessOrEq = false;
    Boolean greater = false;
    Boolean greaterOrEq = false;
    Boolean notEqual = false;

    /**
     * Konstruktoer for RelOpr
     *
     * @param lNum linje nummer
     */
    public RelOpr(int lNum){
        super(lNum);
    }

    @Override
    void check(Block curScope, Library lib) {

    }

    /**
     * identify metode for RelOpr
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<rel opr> on line " + lineNum;
    }

    /**
     * prettyPrint metode for RelOpr
     */
    public void prettyPrint(){
        if(equal) Main.log.prettyPrint(" = ");
        if(notEqual) Main.log.prettyPrint(" <> ");
        if(less) Main.log.prettyPrint(" < ");
        if(lessOrEq) Main.log.prettyPrint(" <= ");
        if(greater) Main.log.prettyPrint(" > ");
        if(greaterOrEq) Main.log.prettyPrint(" >= ");
    }

    @Override
    void genCode(CodeFile f, Block b){
        if(equal){
            f.genInstr("","popl","%ecx","");
            f.genInstr("","cmpl","%eax,%ecx","");
            f.genInstr("","movl","$0,%eax","");
            f.genInstr("","sete","%al","");

        }
        if(notEqual){
            f.genInstr("","popl","%ecx","");
            f.genInstr("","cmpl","%eax,%ecx","");
            f.genInstr("","movl","$0,%eax","");
            f.genInstr("","setne","%al","");
        }
        if(less){
            f.genInstr("","popl","%ecx","");
            f.genInstr("","cmpl","%eax,%ecx","");
            f.genInstr("","movl","$0,%eax","");
            f.genInstr("","setl","%al","");
        }
        if(lessOrEq) {
            f.genInstr("","popl","%ecx","");
            f.genInstr("","cmpl","%eax,%ecx","");
            f.genInstr("","movl","$0,%eax","");
            f.genInstr("","setle","%al","");
        }
        if(greater){
            f.genInstr("","popl","%ecx","");
            f.genInstr("","cmpl","%eax,%ecx","");
            f.genInstr("","movl","$0,%eax","");
            f.genInstr("","setg","%al","");
        }
        if(greaterOrEq){
            f.genInstr("","popl","%ecx","");
            f.genInstr("","cmpl","%eax,%ecx","");
            f.genInstr("","movl","$0,%eax","");
            f.genInstr("","setge","%al","");
        }
    }

    /**
     * metoden parser en RelOpr
     *
     * @param s Scanneren
     * @return et RelOpr objekt
     */
    public static RelOpr parse(Scanner s) {
        enterParser("rel opr");

        RelOpr ro = new RelOpr(s.curLineNum());

        if(s.curToken.kind == equalToken) {
            ro.equal = true;
            s.skip(equalToken);
        }else if(s.curToken.kind == notEqualToken) {
            ro.notEqual = true;
            s.skip(notEqualToken);
        }else if(s.curToken.kind == lessToken){
            ro.less = true;
            s.skip(lessToken);
        }else if(s.curToken.kind == lessEqualToken){
            ro.lessOrEq = true;
            s.skip(lessEqualToken);
        }else if(s.curToken.kind == greaterToken ){
            ro.greater = true;
            s.skip(greaterToken);
        }else {
            ro.greaterOrEq = true;
            s.skip(greaterEqualToken);
        }

        leaveParser("rel opr");
        return ro;
    }
}