package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;
/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for EnumLiteral
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class EnumLiteral extends PascalDecl{


    /**
     * Konstruktoer for EnumLiteral
     *
     * @param lNum linje nummer
     * @param id navn til deklarasjon
     */
    public EnumLiteral(String id, int lNum){
        super(id,lNum);
    }

    @Override
    public void genCode(CodeFile f, Block b) {
        f.genInstr("", "movl", "$" + ((EnumType)type).getIndexOf(this) + ",%eax", "enum " + this.name + " = " + (((EnumType)type)).getIndexOf(this));
    }

    /**
     *
     * @param curScope scope
     * @param lib library
     */
    @Override
    public void check(Block curScope, Library lib) {
        curScope.addDecl(this.name, this);
    }

    /**
     * identify metode for EnumLiteral
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<enum literal> on line " + lineNum;
    }

    /**
     * prettyPrint metode for EnumLiteral
     */
    public void prettyPrint(){
        Main.log.prettyPrint(name);
    }

    /**
     * metoden parser en EnumLiteral
     *
     * @param s Scanneren
     * @return et EnumLiteral objekt
     */
    public static EnumLiteral parse(Scanner s) {
        enterParser("enum literal");

        EnumLiteral el = new EnumLiteral(s.curToken.id ,s.curLineNum());
        s.skip(nameToken);

        leaveParser("enum literal");
        return el;
    }
}