package no.uio.ifi.pascal2100.parser;
import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
/**
 * Created by eska on 16.09.15.
 */

/**
 * klasse for ParamDecl
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class ParamDecl extends PascalDecl {
    TypeName typeName;
    /**
     * Konstruktoer for ParamDecl
     *
     * @param lNum linje nummer
     * @param id navn til deklarasjon
     */
    public ParamDecl(String id, int lNum) {
        super(id, lNum);
    }

    /**
     * genCode for Parameter deklarasjon
     * @param f CodeFile
     * @param b Block
     */
    @Override
    public void genCode(CodeFile f, Block b) {
        f.genInstr("", "movl", (-4*this.declLevel) + "(%ebp),%edx ", "");
        f.genInstr("", "movl", this.declOffset + "(%edx),%eax", this.name + "");
    }

    /**
     * check funksjon for ParamDecl
     *
     * @param curScope scope
     * @param lib library
     */
    public void check(Block curScope, Library lib) {
        curScope.addDecl(this.name, this);
        typeName.check(curScope, lib);
        this.type = typeName.type;
    }

    /**
     * identify metode for ParamDecl
     *
     * @return returnere identify String
     */
    public String identify() {
        return "<param decl> on line " + lineNum;
    }

    /**
     * prettyPrint metode for ParamDecl
     */
    public void prettyPrint() {
        Main.log.prettyPrint(name + " : ");
        typeName.prettyPrint();
    }

    /**
     * metoden parser en ParamDecl
     *
     * @param s Scanneren
     * @return et ParamDecl objekt
     */
    public static ParamDecl parse(Scanner s) {
        enterParser("param decl");
        ParamDecl pd = new ParamDecl(s.curToken.id, s.curLineNum());
        s.skip(nameToken);
        s.skip(colonToken);
        pd.typeName = TypeName.parse(s);
        leaveParser("param decl");
        return pd;
    }
}
