package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;

import javax.lang.model.element.TypeElement;
import java.util.ArrayList;
/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for TypeDeclPart
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class TypeDeclPart extends PascalSyntax{
    ArrayList<TypeDecl> typeDecl;

    /**
     * Konstruktoer for TypeDeclPart
     *
     * @param lNum linje nummer
     */
    public TypeDeclPart(int lNum){
        super(lNum);
        typeDecl = new ArrayList<TypeDecl>();
    }

    /**
     * check metode for TypeDeclPart looper bare gjennom array
     * @param curScope scope
     * @param lib library
     */
    public void check(Block curScope, Library lib) {
        for(TypeDecl td: typeDecl)
            td.check(curScope, lib);
    }

    @Override
    public void genCode(CodeFile f, Block b) {

    }

    /**
     * identify metode for TypeDeclPart
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<type decl part> on line " + lineNum;
    }

    /**
     * prettyPrint metode for TypeDeclPart
     */
    public void prettyPrint(){
        Main.log.prettyPrint("type ");
        for(TypeDecl t: typeDecl) t.prettyPrint();
    }

    /**
     * metoden parser en TypeDeclPart
     *
     * @param s Scanneren
     * @return et TypeDeclPart objekt
     */
    public static TypeDeclPart parse(Scanner s) {
        enterParser("type decl part");

        TypeDeclPart tdp = new TypeDeclPart(s.curLineNum());
        s.skip(typeToken);
        while(s.curToken.kind == nameToken) tdp.typeDecl.add(TypeDecl.parse(s));

        leaveParser("type decl part");
        return tdp;
    }
}