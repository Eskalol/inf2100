package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;
import java.util.ArrayList;
/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for EnumType
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class EnumType extends Type{
    ArrayList<EnumLiteral> enums;

    /**
     * Konstruktoer for EnumType
     *
     * @param lNum linje nummer
     */
    public EnumType(int lNum){
        super(lNum);
        enums = new ArrayList<EnumLiteral>();
    }

    @Override
    public void genCode(CodeFile f, Block b) {

    }

    /**
     * metoden returnere indexen til enumliteral objektet
     * @param el
     * @return
     */
    public int getIndexOf(EnumLiteral el) {
        return enums.indexOf(el);
    }

    /**
     * check metode for enumType
     * @param curScope scope
     * @param lib library
     */
    @Override
    public void check(Block curScope, Library lib) {
        for(EnumLiteral e: enums) {
            e.check(curScope, lib);
            e.type = this;
        }
    }

    /**
     * identify metode for EnumType
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<enum type> on line " + lineNum;
    }

    /**
     * prettyPrint metode for EnumType
     */
    public void prettyPrint(){
        Main.log.prettyPrint("(");
        for(int i = 0; i < enums.size(); i++){
            enums.get(i).prettyPrint();
            if(i < enums.size() - 1) Main.log.prettyPrint(", ");
        }
        Main.log.prettyPrint(")");
    }

    /**
     * metoden parser en EnumType
     *
     * @param s Scanneren
     * @return et EnumType objekt
     */
    public static EnumType parse(Scanner s) {
        enterParser("enum type");

        EnumType et = new EnumType(s.curLineNum());
        s.skip(leftParToken);
        et.enums.add(EnumLiteral.parse(s));
        while(s.curToken.kind == commaToken) {
            s.skip(commaToken);
            et.enums.add(EnumLiteral.parse(s));
        }
        s.skip(rightParToken);

        leaveParser("enum type");
        return et;
    }
}