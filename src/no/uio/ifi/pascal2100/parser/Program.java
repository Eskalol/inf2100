package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;



/**
 * klasse for Program
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class Program extends PascalDecl{
    Block block;

    /**
     * Konstruktoer for Program
     *
     * @param lNum linje nummer
     * @param name navn til deklarasjon
     */
    public Program(int lNum, String name){
        super(name, lNum);
    }

    /**
     * genCode for Program
     * @param f CodeFile
     * @param b Block
     */
    @Override
    public void genCode(CodeFile f, Block b){
        block.offset = 32;
        String progLabel = f.getLabel(name);

        f.genInstr("", ".extern" ,"write_char" ,"");
        f.genInstr("", ".extern" ,"write_int" ,"");
        f.genInstr("", ".extern" ,"write_string" ,"");
        f.genInstr("", ".globl", "_main", "");
        f.genInstr("", ".globl", "main", "");
        f.genInstr("_main","","","");
        f.genInstr("main","call","prog$" + progLabel,"");
        f.genInstr("","movl","$0,%eax","");
        f.genInstr("","ret","","");
        for(PascalDecl pd: block.declarations)
            pd.genCode(f, block);
        f.genInstr("prog$" + progLabel, "enter", "$" + (block.offset + 4 * block.getVarCnt()) + ",$1", "");
        block.genCode(f, b);
        f.genInstr("", "leave", "", "");
        f.genInstr("","ret","","");
    }

    /**
     * check funksjon til Program
     * @param curScope scope
     * @param lib library
     */
    public void check(Block curScope, Library lib) {
        block.check(curScope, lib);
    }
    /**
     * identify metode for Program
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<Program> on line " + lineNum;
    }

    /**
     * prettyPrint metode for Program
     */
    public void prettyPrint(){
        Main.log.prettyPrint("program "); Main.log.prettyPrint(name);
        Main.log.prettyPrint(";"); block.prettyPrint();
        Main.log.prettyPrint(".");
    }

    /**
     * metoden parser en Program
     *
     * @param s Scanneren
     * @return et Program objekt
     */
    public static Program parse(Scanner s) {
        enterParser("program");
        s.skip(programToken);
        Program p = new Program(s.curLineNum(), s.curToken.id);
        s.skip(nameToken);
        s.skip(semicolonToken);
        p.block = Block.parse(s);
        s.skip(dotToken);
        leaveParser("program");
        return p;
    }
}

