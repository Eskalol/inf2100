package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;

import java.util.ArrayList;

/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for SimpleExpr
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class SimpleExpr extends PascalSyntax{
    PrefixOpr prefixOpr;
    ArrayList<Term> term;
    ArrayList<TermOpr> termOpr;
    Type type;
    /**
     * Konstruktoer for SimpleExpr
     *
     * @param lNum linje nummer
     */
    public SimpleExpr(int lNum){
        super(lNum);
        term = new ArrayList<Term>();
        termOpr = new ArrayList<TermOpr>();
    }

    /**
     * check function for SimpleExpr
     * @param curScope scope
     * @param lib library
     */
    public void check(Block curScope, Library lib){
        for (Term t : term)
            t.check(curScope, lib);
        if(term.size() > 0)
            this.type = term.get(0).type;
    }

    /**
     * genCode for SimpleExpr
     * @param f CodeFile
     * @param b Block
     */
    @Override
    void genCode(CodeFile f, Block b){
        term.get(0).genCode(f, b);
        for (int i = 1; i < term.size(); i++) {
            f.genInstr("", "pushl", "%eax", "push eax to stack");
            term.get(i).genCode(f, b);
            termOpr.get(i - 1).genCode(f, b);
        }
        if(prefixOpr != null)
            prefixOpr.genCode(f, b);

    }

    /**
     * identify metode for SimpleExpr
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<simple expr> on line " + lineNum;
    }

    /**
     * prettyPrint metode for SimpleExpr
     */
    public void prettyPrint(){
       prefixOpr.prettyPrint();
       for(int i = 0; i < term.size(); i++){
           term.get(i).prettyPrint();
           if(i < termOpr.size()) termOpr.get(i).prettyPrint();
       }
    }

    /**
     * metoden parser en SimpleExpr
     *
     * @param s Scanneren
     * @return et SimpleExpr objekt
     */
    public static SimpleExpr parse(Scanner s) {
        enterParser("simple expr");

        SimpleExpr se = new SimpleExpr(s.curLineNum());

        if(s.curToken.kind == addToken || s.curToken.kind == subtractToken)
            se.prefixOpr = PrefixOpr.parse(s);

        se.term.add(Term.parse(s));
        while(s.curToken.kind == addToken || s.curToken.kind == subtractToken || s.curToken.kind == orToken){
            se.termOpr.add(TermOpr.parse(s));
            se.term.add(Term.parse(s));
        }

        leaveParser("simple expr");
        return se;
    }
}