package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;

import java.util.ArrayList;

/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for Negation
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class Negation extends PascalSyntax{
    Factor factor;
    Type type;
    /**
     * Konstruktoer for Negation
     *
     * @param lNum linje nummer
     */
    public Negation(int lNum){
        super(lNum);
    }

    /**
     * Negation genCode
     * @param f
     * @param b
     */
    @Override
    public void genCode(CodeFile f, Block b){
        factor.genCode(f, b);
        f.genInstr("","xorl","$1,%eax","");
    }

    /**
     * check function for Negation
     * @param curScope scope
     * @param lib library
     */
    public void check(Block curScope, Library lib){
        factor.check(curScope,lib);
        this.type = factor.type;
    }

    /**
     * identify metode for Negation
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<negation> on line " + lineNum;
    }

    /**
     * prettyPrint metode for Negation
     */
    public void prettyPrint(){
        Main.log.prettyPrint("not ");
        factor.prettyPrint();
    }

    /**
     * metoden parser en Negation
     *
     * @param s Scanneren
     * @return et Negation objekt
     */
    public static Negation parse(Scanner s) {
        enterParser("negation");

        Negation n = new Negation(s.curLineNum());
        s.skip(notToken);
        n.factor = Factor.parse(s);

        leaveParser("negation");
        return n;
    }
}