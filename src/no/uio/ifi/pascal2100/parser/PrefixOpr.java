package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;

import java.util.ArrayList;

/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for PrefixOpr
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class PrefixOpr extends PascalSyntax{
    Boolean plus = false;
    Boolean minus = false;

    /**
     * Konstruktoer for PrefixOpr
     *
     * @param lNum linje nummer
     */
    public PrefixOpr(int lNum){
        super(lNum);
    }

    @Override
    void check(Block curScope, Library lib) {

    }

    /**
     * identify metode for PrefixOpr
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<prefix opr> on line " + lineNum;
    }

    void genCode(CodeFile f, Block b){
        if(minus)
            f.genInstr("","negl","%eax","");
    }

    /**
     * prettyPrint metode for PrefixOpr
     */
    public void prettyPrint(){
        if(plus) Main.log.prettyPrint(" + ");
        if(minus) Main.log.prettyPrint(" - ");
    }

    /**
     * metoden parser en PrefixOpr
     *
     * @param s Scanneren
     * @return et PrefixOpr objekt
     */
    public static PrefixOpr parse(Scanner s) {
        enterParser("prefix opr");

       PrefixOpr po = new PrefixOpr(s.curLineNum());

        if(s.curToken.kind == addToken) {
            po.plus = true;
            s.skip(addToken);
        }else{
            po.minus = true;
            s.skip(subtractToken);
        }
        leaveParser("prefix opr");
        return po;
    }
}