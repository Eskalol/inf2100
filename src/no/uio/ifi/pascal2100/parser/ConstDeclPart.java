package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;
import java.util.ArrayList;
/**
 * Created by Nico on 25.08.2015.
 */


/**
 * klasse for ConstDeclPart
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class ConstDeclPart extends PascalSyntax{
    ArrayList<ConstDecl> constDecl;

    /**
     * Konstruktoer for ConstDeclPart
     *
     * @param lNum linje nummer
     */
    public ConstDeclPart(int lNum){
        super(lNum);
        constDecl = new ArrayList<ConstDecl>();
    }

    @Override
    public void genCode(CodeFile f, Block b) {

    }

    /**
     * checker for ConstDeclPart
     * @param curScope scope
     * @param lib library
     */
    public void check(Block curScope, Library lib) {
        for(ConstDecl cd: constDecl)
            cd.check(curScope, lib);
    }

    /**
     * identify metode for ConstDeclPart
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<const decl part> on line " + lineNum;
    }

    /**
     * prettyPrint metode for ConstDeclPart
     */
    public void prettyPrint(){
        Main.log.prettyPrint("const ");
        for(ConstDecl c: constDecl)
            c.prettyPrint();
    }

    /**
     * metoden parser en ConstDeclPart
     *
     * @param s Scanneren
     * @return et ConstDeclPart objekt
     */
    public static ConstDeclPart parse(Scanner s) {
        enterParser("const decl part");

        ConstDeclPart cdp = new ConstDeclPart(s.curLineNum());
        s.skip(constToken);
        while(s.curToken.kind == nameToken) cdp.constDecl.add(ConstDecl.parse(s));
        leaveParser("const decl part");
        return cdp;
    }
}