package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;
/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for TypeDecl
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class TypeDecl extends PascalDecl{
    TypeName typeName;

    /**
     * Konstruktoer for TypeDecl
     *
     * @param lNum linje nummer
     */
    public TypeDecl(String id, int lNum){
        super(id, lNum);
    }

    @Override
    public void genCode(CodeFile f, Block b) {

    }

    /**
     * check metode for TypeDecl
     * @param curScope scope
     * @param lib library
     */
    public void check(Block curScope, Library lib) {
        curScope.addDecl(this.name, this);
        if(type instanceof EnumType) {
            type.check(curScope, lib);
        }
    }

    /**
     * identify metode for TypeDecl
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<type decl> on line " + lineNum;
    }

    /**
     * prettyPrint metode for TypeDecl
     */
    public void prettyPrint(){
        typeName.prettyPrint(); Main.log.prettyPrint(" = "); type.prettyPrint();
        Main.log.prettyPrintLn(";");
    }

    /**
     * metoden parser en TypeDecl
     *
     * @param s Scanneren
     * @return et TypeDecl objekt
     */
    public static TypeDecl parse(Scanner s) {
        enterParser("type decl");

        TypeDecl td = new TypeDecl(s.curToken.id, s.curLineNum());
        td.typeName = TypeName.parse(s);
        s.skip(equalToken);
        td.type = Type.parse(s);
        s.skip(semicolonToken);

        leaveParser("type decl");
        return td;
    }
}