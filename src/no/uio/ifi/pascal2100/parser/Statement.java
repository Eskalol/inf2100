package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;
/**
 * Created by Nico on 25.08.2015.
 */

/**
 * abstrakt klasse for Statement
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public abstract class Statement extends PascalSyntax{

    public Statement(int lNum){
        super(lNum);
    }

    abstract public String identify();

    abstract public void prettyPrint();

    abstract public void check(Block curScope, Library lib);

    abstract public void genCode(CodeFile f, Block b);
    /**
     * metoden parser en Type
     *
     * @param s Scanneren
     * @return returnerer et objekt som er extendet av Statement
     */
    public static Statement parse(Scanner s) {
        enterParser("statement");
        Statement st;
        if(s.curToken.kind == beginToken) st = CompoundStatement.parse(s);
        else if(s.curToken.kind == whileToken) st = WhileStatement.parse(s);
        else if(s.curToken.kind == ifToken) st = IfStatement.parse(s);
        else if(s.curToken.kind == nameToken && s.nextToken.kind == leftParToken) st = ProcCall.parse(s);
        else if(s.curToken.kind == nameToken && s.nextToken.kind == assignToken) st = AssignStatement.parse(s);
        else if(s.curToken.kind ==  nameToken && s.nextToken.kind == leftBracketToken) st = AssignStatement.parse(s);
        else if(s.curToken.kind == nameToken) st = ProcCall.parse(s);
        else st = EmptyStatement.parse(s);
        leaveParser("statement");
        return st;
    }
}
