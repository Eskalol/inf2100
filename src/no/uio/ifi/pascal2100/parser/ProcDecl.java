package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;
/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for ProcDecl
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class ProcDecl extends PascalDecl{
    Block block;
    ParamDeclList pdl;

    /**
     * Konstruktoer for ProcDecl
     *
     * @param lNum linje nummer
     * @param id navn til deklarasjon
     */
    public ProcDecl(String id, int lNum){
        super(id,lNum);
    }

    /**
     * genCode for ProcDecl
     * @param f CodeFile
     * @param b Block
     */
    @Override
    public void genCode(CodeFile f, Block b) {
        block.offset = 32;
        progProcFuncName = f.getLabel("proc$" + this.name);
        if(pdl != null) {
            pdl.genCode(f, block);
        }

        //block deklalrasjoner
        for(PascalDecl pd: block.declarations)
            pd.genCode(f, block);

        f.genInstr(progProcFuncName, "", "", "");
        f.genInstr("", "enter", "$" + (block.offset + 4 * block.getVarCnt()) + ",$" + (b.getBlocklvl() + 1), "");
        block.genCode(f, b);

        //end of proc
        f.genInstr("", "leave", "", "");
        f.genInstr("", "ret", "", "End of " + this.name);
    }

    /**
     *
     * @param curScope
     * @param lib
     */
    public void check(Block curScope, Library lib) {
        curScope.addDecl(this.name, this);
        block.setOuterScope(curScope);
        if (pdl != null) pdl.check(block, lib);
        block.check(curScope, lib);
    }

    /**
     * identify metode for ProcDecl
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<procedure declaration> on line " + lineNum;
    }

    /**
     * prettyPrint metode for ProcDecl
     */
    public void prettyPrint(){
        Main.log.prettyPrint("procedure "); Main.log.prettyPrint(name);
        if(pdl != null) pdl.prettyPrint();
        Main.log.prettyPrint("; ");
        block.prettyPrint(); Main.log.prettyPrint(";");
    }

    /**
     * metoden parser en ProcDecl
     *
     * @param s Scanneren
     * @return et ProcDecl objekt
     */
    public static ProcDecl parse(Scanner s) {
        enterParser("procedure declaration");

        ProcDecl pd = new ProcDecl(s.nextToken.id, s.curLineNum());
        s.skip(procedureToken);
        s.skip(nameToken);
        if(s.curToken.kind == leftParToken) pd.pdl = ParamDeclList.parse(s);
        s.skip(semicolonToken);
        pd.block = Block.parse(s);
        s.skip(semicolonToken);

        leaveParser("procedure declaration");
        return pd;
    }
}
