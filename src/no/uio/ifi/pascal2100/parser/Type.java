package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;
/**
 * Created by Nico on 25.08.2015.
 */

/**
 * abstrakt klasse for Type
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public abstract class Type extends PascalSyntax{
   Type type;
    public Type(int lNum){
        super(lNum);
    }

    abstract public String identify();
    abstract public void check(Block curScope, Library lib);
    abstract public void prettyPrint();

    /**
     * metoden parser en Type
     *
     * @param s Scanneren
     * @return returnerer et objekt som er extendet av Type
     */
    public static Type parse(Scanner s) {
        enterParser("type");

        Type t;
        if(s.nextToken.kind == rangeToken) t = RangeType.parse(s);
        else if(s.curToken.kind == leftParToken) t = EnumType.parse(s);
        else if(s.curToken.kind == arrayToken ) t = ArrayType.parse(s);
        else t = TypeName.parse(s);

        leaveParser("type");
        return t;
    }
}