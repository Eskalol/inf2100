package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;

import java.util.ArrayList;

/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for FactorOpr
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class FactorOpr extends PascalSyntax{
    Boolean star = false;
    Boolean div = false;
    Boolean mod = false;
    Boolean and = false;

    /**
     * Konstruktoer for FactorOpr
     *
     * @param lNum linje nummer
     */
    public FactorOpr(int lNum){
        super(lNum);
    }

    @Override
    void check(Block curScope, Library lib) {

    }

    /**
     * identify metode for FactorOpr
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<factor opr> on line " + lineNum;
    }


    /**
     * prettyPrint metode for FactorOpr
     */
    public void prettyPrint(){
        if(star) Main.log.prettyPrint(" * ");
        if(div) Main.log.prettyPrint(" div ");
        if(mod) Main.log.prettyPrint(" mod ");
        if(and) Main.log.prettyPrint(" and ");
    }

    /**
     * genCode metode for FactorOpr
     * @param f CodeFile
     * @param b Block
     */
    @Override
    void genCode(CodeFile f, Block b){
        if(star){
            f.genInstr("","movl","%eax,%ecx","");
            f.genInstr("","popl","%eax","");
            f.genInstr("","imull","%ecx,%eax","");
        }
        if(div) {
            f.genInstr("", "movl", "%eax,%ecx", "");
            f.genInstr("", "popl", "%eax", "");
            f.genInstr("", "cdq", "", "");
            f.genInstr("", "idivl", "%ecx", "");
        }
        if(mod) {
            f.genInstr("", "movl", "%eax,%ecx", "mod start");
            f.genInstr("", "popl", "%eax", "");
            f.genInstr("", "cdq", "", "");
            f.genInstr("", "idivl", "%ecx", "");
            f.genInstr("", "movl", "%edx,%eax", "mod end");
        }
        if(and){
            f.genInstr("","movl","%eax,%ecx","");
            f.genInstr("","popl","%eax","");
            f.genInstr("","andl","%ecx,%eax","");
        }
    }

    /**
     * metoden parser en FactorOpr
     *
     * @param s Scanneren
     * @return et FactorOpr objekt
     */
    public static FactorOpr parse(Scanner s) {
        enterParser("factor opr");

        FactorOpr fo = new FactorOpr(s.curLineNum());

        if(s.curToken.kind == multiplyToken){
            fo.star = true;
            s.skip(multiplyToken);
        }else if(s.curToken.kind == divToken){
            fo.div = true;
            s.skip(divToken);
        }else if(s.curToken.kind == modToken){
            fo.mod = true;
            s.skip(modToken);
        }else{
            fo.and = true;
            s.skip(andToken);
        }

        leaveParser("factor opr");
        return fo;
    }
}