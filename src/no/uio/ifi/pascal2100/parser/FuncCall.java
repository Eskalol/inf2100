package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;
import java.util.ArrayList;
/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for FuncCall
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class FuncCall extends PascalSyntax {
    String name;
    ArrayList<Expression> expressions;
    Type type;
    /**
     * Konstruktoer for FuncCall
     *
     * @param lNum linje nummer
     */
    public FuncCall(int lNum) {
        super(lNum);
        expressions = new ArrayList<Expression>();
    }

    /**
     * funcCall genCode metode
     * @param f CodeFile
     * @param b Block
     */
    @Override
    public void genCode(CodeFile f, Block b) {
        String funcName = b.findDecl(this.name).progProcFuncName;
        for(int i = expressions.size()-1; i >= 0; i--) {
            expressions.get(i).genCode(f, b);
            f.genInstr("", "pushl", "%eax", "pushing param #" + (i+1));
        }
        f.genInstr("", "call", funcName, "call function");
        if(expressions.size() > 0)
            f.genInstr("", "addl", "$" + 4*expressions.size() + ",%esp", "cleaning up");
    }

    /**
     *
     * @param curScope scope
     * @param lib library
     */
    public void check(Block curScope, Library lib) {
        this.type = curScope.findDecl(this.name, this).type;
        for(Expression e: expressions)
            e.check(curScope, lib);
    }

    /**
     * identify metode for FuncCall
     *
     * @return returnere identify String
     */
    public String identify() {
        return "<function call> on line " + lineNum;
    }

    /**
     * prettyPrint metode for FuncCall
     */
    public void prettyPrint() {
        Main.log.prettyPrint(name);
        if (expressions != null) {
            Main.log.prettyPrint("(");
            for (int i = 0; i < expressions.size(); i++) {
                expressions.get(i).prettyPrint();
                if (i != expressions.size() - 1) Main.log.prettyPrint(", ");
            }
            Main.log.prettyPrint(")");
        }

    }

    /**
     * metoden parser en FuncCall
     *
     * @param s Scanneren
     * @return et FuncCall objekt
     */
    public static FuncCall parse(Scanner s) {
        enterParser("function call");

        FuncCall fc = new FuncCall(s.curLineNum());

        fc.name = s.curToken.id;
        s.skip(nameToken);

        if (s.curToken.kind == leftParToken) {
            s.skip(leftParToken);
            fc.expressions.add(Expression.parse(s));
            while (s.curToken.kind == commaToken) {
                s.skip(commaToken);
                fc.expressions.add(Expression.parse(s));
            }
            s.skip(rightParToken);
        }

        leaveParser("function call");
        return fc;
    }
}