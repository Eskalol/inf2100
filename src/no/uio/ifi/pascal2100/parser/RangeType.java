package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;
/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for RangeType
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class RangeType extends Type{
    Constant one, two;

    /**
     * Konstruktoer for RangeType
     *
     * @param lNum linje nummer
     */
    public RangeType(int lNum){
        super(lNum);
    }

    @Override
    public void genCode(CodeFile f, Block b) {

    }

    /**
     *
     * @param curScope scope
     * @param lib library
     */
    public void check(Block curScope, Library lib) {
        one.check(curScope, lib);
        two.check(curScope, lib);
    }

    /**
     * identify metode for RangeType
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<range type> on line " + lineNum;
    }

    /**
     * prettyPrint metode for RangeType
     */
    public void prettyPrint(){
        one.prettyPrint(); Main.log.prettyPrint(".."); two.prettyPrint();
    }

    /**
     * metoden parser en RangeType
     *
     * @param s Scanneren
     * @return et RangeType objekt
     */
    public static RangeType parse(Scanner s) {
        enterParser("range type");

        RangeType rt = new RangeType(s.curLineNum());
        rt.one = Constant.parse(s);
        s.skip(rangeToken);
        rt.two = Constant.parse(s);


        leaveParser("range type");
        return rt;
    }
}