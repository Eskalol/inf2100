package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;
/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for Expression
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class Expression extends PascalSyntax{
    SimpleExpr simpleExpr1, simpleExpr2;
    RelOpr relOpr = null;
    Type type;

    /**
     * Konstruktoer for Expression
     *
     * @param lNum linje nummer
     */
    public Expression(int lNum){
        super(lNum);
    }


    /**
     * check function for Expression
     * @param curScope scope
     * @param lib library
     */
    public void check(Block curScope, Library lib){
        if(simpleExpr1!=null) {
            simpleExpr1.check(curScope,lib);
            this.type = simpleExpr1.type;
        }
        if(simpleExpr2!=null) {
            simpleExpr2.check(curScope, lib);
        }
    }

    /**
     * genCode metode for Expression
     * @param f CodeFile
     * @param b Block
     */
    @Override
    public void genCode(CodeFile f, Block b){
        simpleExpr1.genCode(f, b);
        if(relOpr != null) {
            f.genInstr("","pushl","%eax","");
            simpleExpr2.genCode(f, b);
            relOpr.genCode(f, b);
        }
    }

    /**
     * identify metode for Expression
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<Expression> on line " + lineNum;
    }

    /**
     * prettyPrint metode for Expression
     */
    public void prettyPrint(){
       simpleExpr1.prettyPrint();
        if(relOpr != null){
            Main.log.prettyPrint(" ");
            relOpr.prettyPrint();
            Main.log.prettyPrint(" ");
            simpleExpr2.prettyPrint();
        }
    }

    /**
     * metoden parser en Expression
     *
     * @param s Scanneren
     * @return et Expression objekt
     */
    public static Expression parse(Scanner s) {
        enterParser("expression");  
        Expression e = new Expression(s.curLineNum());
        e.simpleExpr1 = SimpleExpr.parse(s);

        if(s.curToken.kind == equalToken || s.curToken.kind == notEqualToken || s.curToken.kind == lessToken || s.curToken.kind == lessEqualToken || s.curToken.kind == greaterToken || s.curToken.kind == greaterEqualToken){
            e.relOpr = RelOpr.parse(s);
            e.simpleExpr2 = SimpleExpr.parse(s);
        }

        leaveParser("expression");
        return e;
    }
}
