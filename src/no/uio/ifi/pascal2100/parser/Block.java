package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;
import java.util.ArrayList;
import java.util.HashMap;
/**
 *
 */

/**
 * klasse for Block
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class Block extends PascalSyntax {
    ConstDeclPart constDeclPart;
    TypeDeclPart typeDeclPart;
    VarDeclPart varDeclPart;
    StatmList statementList;
    ArrayList<ProcDecl> declarations;
    HashMap<String, PascalDecl> decls;
    Block outerScope;

    int blockLvl;
    int offset;

    /**
     * Konstruktoer for Block
     *
     * @param lNum linje nummer
     */
    public Block(int lNum){
        super(lNum);
        declarations = new ArrayList<ProcDecl>();
        decls = new HashMap<String, PascalDecl>();
    }

    public int getVarCnt() {
        if(varDeclPart != null)
            return varDeclPart.getDeclCnt();
        return 0;
    }

    /**
     * Block sin genCode metode
     * @param f CodeFile
     * @param b foreldre Block
     */
    public void genCode(CodeFile f, Block b) {
        if (varDeclPart != null)
            varDeclPart.genCode(f, this);
        statementList.genCode(f, this);
    }

    /**
     * Depricated
     * @param id
     * @return
     */
    public int getBlocklvl(String id) {
        if(decls.get(id) != null)
            return blockLvl;
        return outerScope.getBlocklvl(id);
    }

    /**
     * henter block nivaa
     * @return
     */
    public int getBlocklvl() {
        return blockLvl;
    }

    /**
     * setter block nivaa
     * @param lvl
     */
    public void init_blockLvl(int lvl) {
        blockLvl = lvl;
    }

    /**
     * setter for outerScope trenger den noen ganger
     * @param outerScope
     */
    public void setOuterScope(Block outerScope) {
        this.outerScope = outerScope;
    }

    /**
     * legger til decl til decls hashmap
     * @param id navn p� deklarasjon
     * @param d objektet
     */
    void addDecl(String id, PascalDecl d) {
        if (decls.containsKey(id))
            d.error(id + " declared twice in same block!");
        decls.put(id, d);
    }

    /**
     * check metode for Block
     * @param curScope scope
     * @param lib library
     */
    public void check(Block curScope, Library lib) {
        this.outerScope = curScope;
        init_blockLvl(outerScope.getBlocklvl() + 1);
        if (constDeclPart != null)
            constDeclPart.check(this, lib);
        if (typeDeclPart != null)
            typeDeclPart.check(this, lib);
        if (varDeclPart != null)
            varDeclPart.check(this, lib);
        for(ProcDecl pd: declarations)
            pd.check(this, lib);
        if(statementList != null)
            statementList.check(this, lib);
    }

    /**
     * metoden finner id i hashmap hvis ikke returnerer den et scope opp hvis
     * det ikke er det oeverste scopet
     * hvis id ikke ble funnet i scopet og vi er i det oeverste saa error
     *
     * @param id iden som skal finnes et navn p� deklarajon
     * @param where hvor kallet kommer fra
     * @return
     */
    public PascalDecl findDecl(String id, PascalSyntax where) {
        PascalDecl d = decls.get(id);
        if (d != null) {
            Main.log.noteBinding(id, where, d);
            return d;
        }
        if (outerScope != null)
            return outerScope.findDecl(id,where);
        where.error("Name " + id + " is unknown!");
        return null; // Required by the Java compiler.
    }

    /**
     * findDecl metode for codeGen
     * @param id
     * @return
     */
    public PascalDecl findDecl(String id) {
        PascalDecl d = decls.get(id);
        if (d != null)
            return d;
        return outerScope.findDecl(id);
    }



    /**
     * identify metode for Block
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<Block> on line " + lineNum;
    }

    /**
     * prettyPrint metode for Block
     */
    public void prettyPrint(){
        if(constDeclPart != null) constDeclPart.prettyPrint();
        if(typeDeclPart != null) typeDeclPart.prettyPrint();
        if(varDeclPart != null) varDeclPart.prettyPrint();
        for(ProcDecl p: declarations) p.prettyPrint();
        Main.log.prettyPrintLn();
        Main.log.prettyPrint("begin");
        Main.log.prettyPrintLn(); Main.log.prettyIndent();
        if(statementList != null) statementList.prettyPrint();
        Main.log.prettyOutdent(); Main.log.prettyPrintLn();
        Main.log.prettyPrint("end");
    }

    /**
     * metoden parser en Block
     *
     * @param s Scanneren
     * @return et Block objekt
     */
    public static Block parse(Scanner s) {
        enterParser("block");
        Block b = new Block(s.curLineNum());

        if(s.curToken.kind == constToken) b.constDeclPart = ConstDeclPart.parse(s);
        if(s.curToken.kind == typeToken) b.typeDeclPart = TypeDeclPart.parse(s);
        if(s.curToken.kind == varToken) b.varDeclPart = VarDeclPart.parse(s);
        while(s.curToken.kind == procedureToken || s.curToken.kind == functionToken) {
            if(s.curToken.kind == procedureToken) b.declarations.add(ProcDecl.parse(s));
            else if(s.curToken.kind == functionToken) b.declarations.add(FuncDecl.parse(s));
        }
        s.skip(beginToken);
        b.statementList = StatmList.parse(s);
        s.skip(endToken);

        leaveParser("block");
        return b;
    }
}

