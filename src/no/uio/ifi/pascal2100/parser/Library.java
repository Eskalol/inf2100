package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.CodeFile;

/**
 * Created by Eskil on 03.11.2015.
 */
public class Library extends Block{

    /**
     * Konstruktoer for Library
     *
     * @param lNum linje nummer
     */
    public Library(int lNum) {
        super(lNum);
        //adding decls
        this.init_blockLvl(0);

        //integer
        TypeDecl integer = new TypeDecl("integer", 0);
        integer.typeName = new TypeName("integer", 0);
        integer.type = integer.typeName;
        this.addDecl("integer", integer);

        //char
        TypeDecl chr = new TypeDecl("char", 0);
        chr.typeName = new TypeName("char", 0);
        chr.type = chr.typeName;
        this.addDecl("char", chr);

        //eol
        ConstDecl eol = new ConstDecl(0, "eol");
        eol.constant = new Constant(0);
        eol.constant.string = new CharLiteral(0);
        ((CharLiteral)eol.constant.string).c = (char)10;
        eol.type = new TypeName("char", 0);
        this.addDecl("eol", eol);

        //Boolean
        TypeDecl bool = new TypeDecl("Boolean", 0);
        bool.typeName = new TypeName("Boolean", 0);
        bool.type = new EnumType(0);
        EnumLiteral f = new EnumLiteral("false", 0);
        f.type = bool.type;
        this.addDecl("false", f);
        ((EnumType) bool.type).enums.add(f);

        EnumLiteral t = new EnumLiteral("true", 0);
        t.type = bool.type;
        this.addDecl("true", t);
        ((EnumType)bool.type).enums.add(t);
        this.addDecl("Boolean", bool);

        this.addDecl("write", new ProcDecl("write", 0));

    }

    @Override
    public void genCode(CodeFile f, Block b) {

    }
}
