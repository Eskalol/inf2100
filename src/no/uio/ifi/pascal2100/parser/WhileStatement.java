package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;
/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for WhileStatement
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class WhileStatement extends Statement{
    Expression expr;
    Statement body;

    /**
     * Konstruktoer for WhileStatement
     *
     * @param lNum linje nummer
     */
    public WhileStatement(int lNum){
        super(lNum);
    }

    /**
     * check function for WhileStatement
     * @param curScope scope
     * @param lib library
     */
    public void check(Block curScope, Library lib){
        if(expr!=null) expr.check(curScope,lib);
        if(body!=null) body.check(curScope,lib);
    }

    /**
     * genCode for whilestmt
     * @param f CodeFile
     * @param b Block
     */
    @Override
    public void genCode(CodeFile f, Block b){
        String label1 = f.getLocalLabel();
        String label2 = f.getLocalLabel();

        f.genInstr(label1, "", "", "Start while-statement");
        expr.genCode(f, b);
        f.genInstr("", "cmpl", "$0,%eax","");
        f.genInstr("", "je", label2, "");
        body.genCode(f, b);
        f.genInstr("", "jmp", label1, "");
        f.genInstr(label2, "", "", "End while-statement");
    }

    /**
     * identify metode for WhileStatement
     *
     * @return returnere identify String
     */
    @Override
    public String identify(){
        return "<while-statement> on line " + lineNum;
    }

    /**
     * prettyPrint metode for WhileStatement
     */
    @Override
    public void prettyPrint(){
        Main.log.prettyPrintLn();
        Main.log.prettyPrint("while "); expr.prettyPrint();
        Main.log.prettyPrint(" do "); body.prettyPrint();
    }

    /**
     * metoden parser en WhileStatement
     *
     * @param s Scanneren
     * @return et WhileStatement objekt
     */
    public static WhileStatement parse(Scanner s) {
        enterParser("while-statm");

        WhileStatement ws = new WhileStatement(s.curLineNum());
        s.skip(whileToken);
        ws.expr = Expression.parse(s);
        s.skip(doToken);
        ws.body = Statement.parse(s);

        leaveParser("while-statm");
        return ws;
    }
}
