package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;
import java.util.ArrayList;
import java.util.ArrayList;

/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for Term
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class Term extends PascalSyntax{
    ArrayList<Factor> factors;
    ArrayList<FactorOpr> factorOprs;
    Type type;
    /**
     * Konstruktoer for Term
     *
     * @param lNum linje nummer
     */
    public Term(int lNum){
        super(lNum);
        factors = new ArrayList<Factor>();
        factorOprs = new ArrayList<FactorOpr>();
    }

    /**
     * genCode for Term
     * @param f CodeFile
     * @param b Block
     */
    @Override
    public void genCode(CodeFile f, Block b){
        factors.get(0).genCode(f, b);
        for(int i = 1; i < factors.size(); i++){
            f.genInstr("", "pushl", "%eax", "");
            factors.get(i).genCode(f, b);
            factorOprs.get(i -1).genCode(f, b);
        }
    }

    /**
     * check function for Term
     * @param curScope scope
     * @param lib library
     */
    public void check(Block curScope, Library lib){
        for(Factor f: factors)
            f.check(curScope,lib);
        this.type = factors.get(0).type;
    }

    /**
     * identify metode for Term
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<term> on line " + lineNum;
    }

    /**
     * prettyPrint metode for Term
     */
    public void prettyPrint(){
        for(int i = 0; i < factors.size(); i++){
            factors.get(i).prettyPrint();
            if(i < factorOprs.size()) factorOprs.get(i).prettyPrint();
        }
    }

    /**
     * metoden parser en Term
     *
     * @param s Scanneren
     * @return et Term objekt
     */
    public static Term parse(Scanner s) {
        enterParser("term");
        Term t = new Term(s.curLineNum());
        t.factors.add(Factor.parse(s));
        while(s.curToken.kind == multiplyToken || s.curToken.kind == divToken || s.curToken.kind == modToken || s.curToken.kind == andToken) {
            t.factorOprs.add(FactorOpr.parse(s));
            t.factors.add(Factor.parse(s));
        }

        leaveParser("term");
        return t;
    }
}