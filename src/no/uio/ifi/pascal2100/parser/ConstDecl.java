package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;
/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for ConstDecl
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class ConstDecl extends PascalDecl{
    Constant constant;

    /**
     * Konstruktoer for ConstDecl
     *
     * @param lNum linje nummer
     * @param id navn til deklarasjon
     */
    public ConstDecl(int lNum, String id){
        super(id, lNum);
    }

    /**
     * genCode metode for ConstDecl
     * @param f CodeFile
     * @param b Block
     */
    @Override
    public void genCode(CodeFile f, Block b) {
        constant.genCode(f, b);
    }

    /**
     * Adding decl name to decls in curScope
     * @param curScope
     * @param lib
     */
    public void check(Block curScope, Library lib) {
        curScope.addDecl(this.name, this);
        constant.check(curScope, lib);
        this.type = constant.type;
    }

    /**
     * identify metode for ConstDecl
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<const decl> on line " + lineNum;
    }

    /**
     * prettyPrint metode for ConstDecl
     */
    public void prettyPrint(){
        Main.log.prettyPrint(name); Main.log.prettyPrint(" = ");
        constant.prettyPrint(); Main.log.prettyPrintLn(";");
    }

    /**
     * metoden parser en ConstDecl
     *
     * @param s Scanneren
     * @return et ConstDecl objekt
     */
    public static ConstDecl parse(Scanner s) {
        enterParser("const decl");

        ConstDecl cd = new ConstDecl(s.curLineNum(), s.curToken.id);
        s.skip(nameToken);
        s.skip(equalToken);
        cd.constant = Constant.parse(s);
        s.skip(semicolonToken);

        leaveParser("const decl");
        return cd;
    }
}