package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;

import java.util.ArrayList;

/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for InnerExpr
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class InnerExpr extends PascalSyntax{
    Expression expression;
    Type type;
    /**
     * Konstruktoer for InnerExpr
     *
     * @param lNum linje nummer
     */
    public InnerExpr(int lNum){
        super(lNum);
    }

    /**
     * check function for InnerExpr
     * @param curScope scope
     * @param lib lib
     */
    public void check(Block curScope, Library lib){
        expression.check(curScope,lib);
        this.type = expression.type;
    }

    /**
     * InnerExpr genCode
     * @param f CodeFile
     * @param b Block
     */
    @Override
    void genCode(CodeFile f, Block b){
        expression.genCode(f, b);
    }

    /**
     * identify metode for InnerExpr
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<inner expression> on line " + lineNum;
    }

    /**
     * prettyPrint metode for InnerExpr
     */
    public void prettyPrint(){
        Main.log.prettyPrint("(");
        expression.prettyPrint();
        Main.log.prettyPrint(")");
    }

    /**
     * metoden parser en InnerExpr
     *
     * @param s Scanneren
     * @return et InnerExpr objekt
     */
    public static InnerExpr parse(Scanner s) {
        enterParser("inner expression");

        InnerExpr ie = new InnerExpr(s.curLineNum());
        s.skip(leftParToken);
        ie.expression = Expression.parse(s);
        s.skip(rightParToken);

        leaveParser("inner expression");
        return ie;
    }
}