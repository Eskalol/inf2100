package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;

import javax.naming.ldap.ExtendedRequest;
import java.util.ArrayList;
/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for ProcCall
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class ProcCall extends Statement{
    ArrayList<Expression> expressions;
    String name;

    /**
     * Konstruktoer for ProcCall
     *
     * @param lNum linje nummer
     */
    public ProcCall(int lNum){
        super(lNum);
        expressions = new ArrayList<Expression>();
    }

    /**
     * ProcCall sin check metode
     * @param curScope scope
     * @param lib library
     */
    public void check(Block curScope, Library lib) {
        curScope.findDecl(this.name, this);
        for(Expression e: expressions)
            e.check(curScope, lib);
    }

    /**
     * metoden bllir kalt paa hvis det er biblioteksfunksjonen writes
     * @param f codefile
     * @param b blocken den tilhoerer
     */
    public void genWrite(CodeFile f, Block b) {
        for(Expression e: expressions) {
            if(e.type instanceof TypeName) {
                switch (((TypeName)e.type).name) {
                    case "string" :
                        e.genCode(f, b);
                        f.genInstr("", "pushl", "%eax", "push param #" + (expressions.indexOf(e)+1));
                        f.genInstr("", "call", "write_string", "");
                        f.genInstr("", "addl", "$4,%esp", "pop param");
                        break;
                    case "integer":
                        e.genCode(f, b);
                        f.genInstr("", "pushl", "%eax", "push param #" + (expressions.indexOf(e)+1));
                        f.genInstr("", "call", "write_int", "");
                        f.genInstr("", "addl", "$4,%esp", "pop param");
                        break;
                    case "char":
                        e.genCode(f, b);
                        f.genInstr("", "pushl", "%eax", "push param #" + (expressions.indexOf(e)+1));
                        f.genInstr("", "call", "write_char", "");
                        f.genInstr("", "addl", "$4,%esp", "pop param");
                        break;
                    case "Boolean":
                        e.genCode(f, b);
                        f.genInstr("", "pushl", "%eax", "push param #" + (expressions.indexOf(e)+1));
                        f.genInstr("", "call", "write_int", "");
                        f.genInstr("", "addl", "$4,%esp", "pop param");
                        break;
                }
            }
        }
    }

    /**
     * genCode for ProcCalll
     * @param f CodeFile
     * @param b Block
     */
    @Override
    public void genCode(CodeFile f, Block b) {
        if(name.equals("write")) {
            genWrite(f, b);
            return;
        }
        String procName = b.findDecl(this.name).progProcFuncName;
        for(int i = expressions.size()-1; i >= 0; i--) {
            expressions.get(i).genCode(f, b);
            f.genInstr("", "pushl", "%eax", "pushing param #" + (i+1));
        }
        f.genInstr("", "call", procName, "call proc");
        if(expressions.size() > 0)
            f.genInstr("", "addl", "$" + 4*expressions.size() + ",%esp", "cleaning up");
    }

    /**
     * identify metode for ProcCall
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<procedure call> on line " + lineNum;
    }

    /**
     * prettyPrint metode for ProcCall
     */
    @Override
    public void prettyPrint(){
        Main.log.prettyPrintLn();
        Main.log.prettyPrint(name);
        if(expressions != null){
            Main.log.prettyPrint("(");
            for(int i = 0; i < expressions.size(); i++){
                expressions.get(i).prettyPrint();
                if(i < expressions.size() - 1) Main.log.prettyPrint(", ");
            }
            Main.log.prettyPrint(")");
        }
    }

    /**
     * metoden parser en ProcCall
     *
     * @param s Scanneren
     * @return et ProcCall objekt
     */
    public static ProcCall parse(Scanner s) {
        enterParser("proc call");

        ProcCall pc = new ProcCall(s.curLineNum());
        pc.name = s.curToken.id;
        s.skip(nameToken);
        if(s.curToken.kind == leftParToken){
            s.skip(leftParToken);
            while(s.curToken.kind != rightParToken){
                pc.expressions.add(Expression.parse(s));
                if(s.curToken.kind == commaToken) s.skip(commaToken);
            }
            s.skip(rightParToken);
        }
        leaveParser("proc call");
        return pc;
    }
}