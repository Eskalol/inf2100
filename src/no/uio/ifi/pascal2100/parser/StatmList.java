package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;

import java.util.ArrayList;
/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for StatmList
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class StatmList extends PascalSyntax{
    ArrayList<Statement> statements;

    /**
     * Konstruktoer for StatmList
     *
     * @param lNum linje nummer
     */
    public StatmList(int lNum){
        super(lNum);
        statements = new ArrayList<Statement>();
    }

    /**
     * check function for StatmList
     * @param curScope scope
     * @param lib library
     */
    public void check(Block curScope, Library lib){
        for (Statement s : statements)
            s.check(curScope, lib);
    }

    /**
     * genCode for StatmList
     * @param f CodeFile
     * @param b Block
     */
    @Override
    public void genCode(CodeFile f, Block b) {
        for (Statement s: statements)
            s.genCode(f, b);
    }

    /**
     * identify metode for StatmList
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<statm list> on line " + lineNum;
    }

    /**
     * prettyPrint metode for StatmList
     */
    public void prettyPrint(){
        for(Statement s: statements)
            s.prettyPrint();
    }

    /**
     * metoden parser en StatmList
     *
     * @param s Scanneren
     * @return et StatmList objekt
     */
    public static StatmList parse(Scanner s) {
        enterParser("statm list");

        StatmList sl = new StatmList(s.curLineNum());
        while(s.curToken.kind != endToken)
            sl.statements.add(Statement.parse(s));
        leaveParser("statm list");

        return sl;
    }
}