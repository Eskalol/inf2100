package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;
import java.util.ArrayList;
/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for CompoundStatement
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class CompoundStatement extends Statement{
   StatmList statements;


    /**
     * Konstruktoer for CompoundStatement
     *
     * @param lNum linje nummer
     */
    public CompoundStatement(int lNum){
        super(lNum);
    }


    /**
     *  check function for CompundStatement
     * @param curScope
     * @param lib
     */
    @Override
    public void check(Block curScope, Library lib){
        if(statements != null)
            statements.check(curScope,lib);
    }

    public void genCode(CodeFile f, Block b) {
        statements.genCode(f, b);
    }

    /**
     * identify metode for CompoundStatement
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<compund statement> on line " + lineNum;
    }

    /**
     * prettyPrint metode for CompoundStatement
     */
    @Override
    public void prettyPrint(){
        Main.log.prettyPrint("begin");
        Main.log.prettyPrintLn();
        Main.log.prettyIndent();
        statements.prettyPrint();
        Main.log.prettyOutdent();
        Main.log.prettyPrintLn();
        Main.log.prettyPrint("end");
    }

    /**
     * metoden parser en CompoundStatement
     *
     * @param s Scanneren
     * @return et CompoundStatement objekt
     */
    public static CompoundStatement parse(Scanner s) {
        enterParser("compound statement");
        CompoundStatement cs = new CompoundStatement(s.curLineNum());

        s.skip(beginToken);
        cs.statements = StatmList.parse(s);
        s.skip(endToken);

        leaveParser("compound statement");
        return cs;
    }
}
