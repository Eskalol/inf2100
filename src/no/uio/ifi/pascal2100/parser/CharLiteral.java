package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;

import java.util.ArrayList;
/**
 * Created by Nico on 19/10/15.
 */

/**
 * klasse for CharLiteral
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class CharLiteral extends Literal {
     char c;

    CharLiteral(int lineNum){ super(lineNum); }

    @Override
    void check(Block curScope, Library lib) {
        this.type = new TypeName("char", 0);
    }

    /**
     * metoden setter en char verdi i eax
     * @param f CodeFile
     * @param b Block
     */
    @Override
    public void genCode(CodeFile f, Block b) {
        f.genInstr("", "movl", "$" + (int)c + ",%eax", "char " + (int)c);
    }

    /**
     * identify metode for CharLiteral
     *
     * @return returnere identify String
     */
    public String identify(){ return "<char literal> on line " + lineNum;}

    /**
     * prettyPrint metode for CharLiteral
     */
    public void prettyPrint(){ Main.log.prettyPrint("'" + c + "'");}


    /**
     * metoden parser en CharLiteral
     *
     * @param s Scanneren
     * @return et CharLiteral objekt
     */
    public static CharLiteral parse(Scanner s){
        enterParser("char literal");
        CharLiteral cl = new CharLiteral(s.curLineNum());

        cl.c = s.curToken.strVal.charAt(0);
        s.skip(stringValToken);
        leaveParser("char literal");
        return cl;
    }

}
