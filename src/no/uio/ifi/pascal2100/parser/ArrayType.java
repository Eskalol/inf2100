package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;


/**
 * klasse for ArrayType
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class ArrayType extends Type{
    Type one, two;

    /**
     * Konstruktoer for ArrayType
     *
     * @param lNum linje nummer
     */
    public ArrayType(int lNum){
        super(lNum);
    }

    public void genCode(CodeFile f, Block b) {
    }

    /**
     * ArrayType sin check
     * @param curScope scope
     * @param lib library
     */
    public void check(Block curScope, Library lib) {
        one.check(curScope, lib);
        two.check(curScope, lib);
    }

    /**
     * identify metode for ArrayType
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<array type> on line " + lineNum;
    }

    /**
     * prettyPrint metode for ArrayType
     */
    public void prettyPrint(){
        Main.log.prettyPrint("array ["); one.prettyPrint();
        Main.log.prettyPrint("] of ");  two.prettyPrint();
    }

    /**
     * metoden parser en ArrayType
     *
     * @param s Scanneren
     * @return et ArrayType objekt
     */
    public static ArrayType parse(Scanner s) {
        enterParser("array type");

        ArrayType at = new ArrayType(s.curLineNum());
        s.skip(arrayToken);
        s.skip(leftBracketToken);
        at.one = Type.parse(s);
        s.skip(rightBracketToken);
        s.skip(ofToken);
        at.two = Type.parse(s);
        leaveParser("array type");
        return at;
    }
}