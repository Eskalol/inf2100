package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;
/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for IfStatement
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class IfStatement extends Statement{
    Expression expr;
    Statement body, elseBody = null;

    /**
     * Konstruktoer for IfStatement
     *
     * @param lNum linje nummer
     */
    public IfStatement(int lNum){
        super(lNum);
    }

    /**
     * check function for IfStatement
     * @param curScope scope
     * @param lib library
     */
    public void check(Block curScope, Library lib){
        expr.check(curScope,lib);
        body.check(curScope,lib);
        if(elseBody != null) elseBody.check(curScope,lib);
    }

    /**
     * genCode metode for IfStatment
     * @param f CodeFille
     * @param b Block
     */
    @Override
    public void genCode(CodeFile f, Block b) {
        //labels
        String endLabel, elseLabel = "";
        f.genInstr("", "", "", "Start if-statement");
        expr.genCode(f, b);
        f.genInstr("", "cmpl", "$0,%eax", "");
        if(elseBody != null) {
            elseLabel = f.getLocalLabel();
            endLabel = f.getLocalLabel();
            f.genInstr("", "je", elseLabel, "");
            body.genCode(f, b);
            f.genInstr("", "jmp", endLabel, "");
            f.genInstr(elseLabel, "", "", "else If-statement");
            elseBody.genCode(f, b);
            f.genInstr(endLabel, "", "", "end If-statement");
        } else {
            endLabel = f.getLocalLabel();
            f.genInstr("", "je", endLabel, "");
            body.genCode(f, b);
            f.genInstr(endLabel, "", "", "end If-statement");
        }

    }

    /**
     * identify metode for IfStatement
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<if-statement> on line " + lineNum;
    }

    /**
     * prettyPrint metode for IfStatement
     */
    public void prettyPrint(){
        Main.log.prettyPrint("if "); expr.prettyPrint();
        Main.log.prettyPrintLn(" then "); body.prettyPrint();
        if(elseBody != null) {
            Main.log.prettyPrint(" else ");
            elseBody.prettyPrint();
        }
    }

    /**
     * metoden parser en IfStatement
     *
     * @param s Scanneren
     * @return et IfStatement objekt
     */
    public static IfStatement parse(Scanner s) {
        enterParser("if-statm");

        IfStatement is = new IfStatement(s.curLineNum());
        s.skip(ifToken);
        is.expr = Expression.parse(s);
        s.skip(thenToken);
        is.body = Statement.parse(s);
        if(s.curToken.kind == elseToken){
            s.skip(elseToken);
            is.elseBody = Statement.parse(s);
        }

        leaveParser("if-statm");
        return is;
    }
}
