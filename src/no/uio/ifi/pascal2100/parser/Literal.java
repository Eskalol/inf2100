package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;

import java.util.ArrayList;
/**
 * Created by Nico on 19/10/15.
 */

/**
 * abstrakt klasse for Literal
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public abstract class Literal extends PascalSyntax {
    Type type;
    Literal(int lnum){
        super(lnum);
    }

    abstract public String identify();
    abstract public void prettyPrint();
    abstract public void genCode(CodeFile f, Block b);
    /**
     * metoden parser en Literal
     *
     * @param s Scanneren
     * @return returnerer et objekt som er extendet av Literal
     */
    static public Literal parse(Scanner s){
        if(s.curToken.strVal.length()  == 1)
            return CharLiteral.parse(s);
        else return StringLiteral.parse(s);
    }
}
