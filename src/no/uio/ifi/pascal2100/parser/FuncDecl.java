package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;
/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for FuncDecl
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class FuncDecl extends ProcDecl{
    TypeName typeName;
    /**
     * Konstruktoer for FuncDecl
     *
     * @param lNum linje nummer
     * @param id navn til deklarasjon
     */
    public FuncDecl(String id, int lNum){
        super(id,lNum);
    }

    /**
     * metoden genererer kode for en funksjon deklarasjon
     * @param f CodeFile
     * @param b Block
     */
    @Override
    public void genCode(CodeFile f, Block b) {
        this.declOffset = 32;
        this.declLevel = b.getBlocklvl();
        block.offset = 32;
        progProcFuncName = f.getLabel("func$" + this.name);
        if(pdl != null) {
            pdl.genCode(f, block);
        }

        //block deklalrasjoner
        for(PascalDecl pd: block.declarations)
            pd.genCode(f, block);

        f.genInstr(progProcFuncName, "", "", "");
        f.genInstr("", "enter", "$" + (block.offset + 4 * block.getVarCnt()) + ",$" + (b.getBlocklvl() + 1), "");
        block.genCode(f, b);
        //end of function
        f.genInstr("", "movl", -block.offset + "(%ebp),%eax", "");
        f.genInstr("", "leave", "", "");
        f.genInstr("", "ret", "", "End of " + this.name);

    }

    /**
     *
     * @param curScope scope
     * @param lib library
     */
    @Override
    public void check(Block curScope, Library lib) {
        typeName.check(curScope, lib);
        this.type = typeName.type;
        super.check(curScope, lib);
    }

    /**
     * identify metode for FuncDecl
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<function declaration> on line " + lineNum;
    }

    /**
     * prettyPrint metode for FuncDecl
     */
    @Override
    public void prettyPrint(){
        Main.log.prettyPrint("function "); Main.log.prettyPrint(name);
        if(pdl != null) pdl.prettyPrint();
        Main.log.prettyPrint(" : ");
        typeName.prettyPrint(); Main.log.prettyPrint("; ");
        block.prettyPrint(); Main.log.prettyPrint("; ");
    }

    /**
     * metoden parser en FuncDecl
     *
     * @param s Scanneren
     * @return et FuncDecl objekt
     */
    public static FuncDecl parse(Scanner s) {
        enterParser("function declaration");

        FuncDecl fd = new FuncDecl(s.nextToken.id, s.curLineNum());
        s.skip(functionToken);
        s.skip(nameToken);
        if(s.curToken.kind == leftParToken) fd.pdl = ParamDeclList.parse(s);
         s.skip(colonToken);
        fd.typeName = TypeName.parse(s);
        fd.type = fd.typeName;
        s.skip(semicolonToken);
        fd.block = Block.parse(s);
        s.skip(semicolonToken);

        leaveParser("function declaration");
        return fd;
    }
}
