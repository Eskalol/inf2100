package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;

import java.util.ArrayList;

/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for TermOpr
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class TermOpr extends PascalSyntax{
    Boolean plus = false;
    Boolean minus = false;
    Boolean or = false;

    /**
     * Konstruktoer for TermOpr
     *
     * @param lNum linje nummer
     */
    public TermOpr(int lNum){
        super(lNum);
    }

    @Override
    void check(Block curScope, Library lib) {

    }

    /**
     * identify metode for TermOpr
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<term opr> on line " + lineNum;
    }

    void genCode(CodeFile f, Block b) {
        f.genInstr("", "movl", "%eax,%ecx", "move eax to ecx");
        f.genInstr("", "popl", "%eax", "pop eax");
        if(plus)
            f.genInstr("", "addl", "%ecx,%eax", "eax = eax + ecx");
        else if(minus)
            f.genInstr("", "subl", "%ecx,%eax", "eax = eax - ecx");
        else
            f.genInstr("", "orl", "%ecx,%eax", "");
    }

    /**
     * prettyPrint metode for TermOpr
     */
    public void prettyPrint(){
        if(plus) Main.log.prettyPrint(" + ");
        if(minus) Main.log.prettyPrint(" - ");
        if(or) Main.log.prettyPrint(" or ");
    }

    /**
     * metoden parser en TermOpr
     *
     * @param s Scanneren
     * @return et TermOpr objekt
     */
    public static TermOpr parse(Scanner s) {
        enterParser("term opr");

        TermOpr to = new TermOpr(s.curLineNum());

        if(s.curToken.kind == addToken){
            s.skip(addToken);
            to.plus = true;
        }else if(s.curToken.kind == subtractToken){
            s.skip(subtractToken);
            to.minus = true;
        }else{
            s.skip(orToken);
            to.or = true;
        }

        leaveParser("term opr");
        return to;
    }
}