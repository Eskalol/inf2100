package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;
/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for Constant
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class Constant extends PascalSyntax{
    String name = null;
    NumericLiteral number = null;
    Literal string = null;
    Type type;
    /**
     * Konstruktoer for Constant
     *
     * @param lNum linje nummer
     */
    public Constant(int lNum){
        super(lNum);
    }

    /**
     * genCode metode for en konstant
     * @param f CodeFile
     * @param b Block
     */
    @Override
    public void genCode(CodeFile f, Block b){
        if(name != null){
            b.findDecl(name).genCode(f, b);
        }
        if(number != null){
            number.genCode(f, b);
        }
        if(string != null){
            string.genCode(f, b);
        }
    }

    /**
     *
     * @param curScope scope
     * @param lib library
     */
    public void check(Block curScope, Library lib) {
        if(this.name != null) {
            this.type = curScope.findDecl(this.name, this).type;
        }
        if(this.number != null )  {
            number.check(curScope, lib);
            this.type = new TypeName("integer", 0);
        }
        if (this.string != null ) {
            string.check(curScope, lib);
            this.type = string.type;
        }
    }

    /**
     * identify metode for Constant
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<constant> on line " + lineNum;
    }

    /**
     * prettyPrint metode for Constant
     */
    public void prettyPrint(){
        if(name != null) Main.log.prettyPrint(name);
        if(number != null) number.prettyPrint();
        if(string != null) string.prettyPrint();
    }

    /**
     * metoden parser en Constant
     *
     * @param s Scanneren
     * @return et Constant objekt
     */
    public static Constant parse(Scanner s) {
        enterParser("constant");

        Constant c = new Constant(s.curLineNum());
        if(s.curToken.kind == stringValToken) {
            c.string = Literal.parse(s);
        }else if(s.curToken.kind == intValToken){
            c.number = NumericLiteral.parse(s);
        }else{
            c.name = s.curToken.id;
            s.skip(nameToken);
        }

        leaveParser("constant");
        return c;
    }
}