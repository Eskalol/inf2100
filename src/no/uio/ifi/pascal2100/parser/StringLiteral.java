package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;
/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for StringLiteral
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class StringLiteral extends Literal{
    String val;

    /**
     * Konstruktoer for StringLiteral
     *
     * @param lNum linje nummer
     */
    public StringLiteral(int lNum){
        super(lNum);
    }

    /**
     * check metode for stringliteral setter type
     * @param curScope
     * @param lib
     */
    @Override
    void check(Block curScope, Library lib) {
        this.type = new TypeName("string", 0);
    }

    /**
     * genCode for StringLiteral
     * @param f CodeFile
     * @param b Block
     */
    @Override
    public void genCode(CodeFile f, Block b) {
        String label = f.getLocalLabel();
        f.genString(label, val, "");
        f.genInstr("", "leal", label + ",%eax", "");

    }

    /**
     * identify metode for StringLiteral
     *
     * @return returnere identify String
     */
    public String identify(){
        return "<string literal> on line " + lineNum;
    }

    /**
     * prettyPrint metode for StringLiteral
     */
    public void prettyPrint(){
        Main.log.prettyPrint("'");
        Main.log.prettyPrint(val);
        Main.log.prettyPrint("'");
    }

    /**
     * metoden parser en StringLiteral
     *
     * @param s Scanneren
     * @return et StringLiteral objekt
     */
    public static StringLiteral parse(Scanner s) {
        enterParser("string literal");

        StringLiteral sl = new StringLiteral(s.curLineNum());
        sl.val = s.curToken.strVal;
        s.skip(stringValToken);

        leaveParser("string literal");
        return sl;
    }
}