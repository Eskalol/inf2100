package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;
/**
 *
 */

/**
 * klasse for AssignStatement
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class AssignStatement extends Statement{
    Variable var;
    Expression exp;

    /**
     * Konstruktoer for AssignStatement
     *
     * @param lNum linje nummer
     */
    public AssignStatement(int lNum){
        super(lNum);
    }

    /**
     * genrere kode for assignstmt,
     * @param f CodeFile
     * @param b blocken stmt hoerer til
     */
    @Override
    public void genCode(CodeFile f, Block b) {
        exp.genCode(f, b);
        PascalDecl decl = b.findDecl(var.name);
        if(decl instanceof FuncDecl) {
            //liten hacky loesning for aa faa returverdi i funksjoner
            f.genInstr("", "movl", "%eax," + -decl.declOffset + "(%ebp)", var.name + ":= ");
        } else {
            f.genInstr("", "movl", (-4*decl.declLevel) + "(%ebp),%edx", "");
            f.genInstr("", "movl", "%eax," + -decl.declOffset + "(%edx)", var.name + ":= ");
        }
    }

    /**
     * check metode for AssignStatement
     * @param curScope scope
     * @param lib library
     */
    public void check(Block curScope, Library lib) {
        var.check(curScope, lib);
        exp.check(curScope, lib);
    }
    /**
     * identify metode for AssignStatement
     *
     * @return returnere identify String
     */
    @Override
    public String identify(){
        return "<assign-statement> on line " + lineNum;
    }

    /**
     * prettyPrint metode for AssignStatement
     */
     @Override
    public void prettyPrint(){
        var.prettyPrint(); Main.log.prettyPrint(" := "); exp.prettyPrint();
    }

    /**
     * metoden parser en AssignStatement
     *
     * @param s Scanneren
     * @return et AssignStatement objekt
     */
    public static AssignStatement parse(Scanner s) {
        enterParser("assign-statm");

        AssignStatement as = new AssignStatement(s.curLineNum());
        as.var = Variable.parse(s);
        s.skip(assignToken);
        as.exp = Expression.parse(s);
        leaveParser("assign-statm");
        return as;
    }
}