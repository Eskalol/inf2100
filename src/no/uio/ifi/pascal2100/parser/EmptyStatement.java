package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import no.uio.ifi.pascal2100.scanner.Token;
/**
 * Created by Nico on 25.08.2015.
 */

/**
 * klasse for EmptyStatement
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class EmptyStatement extends Statement{

    /**
     * Konstruktoer for EmptyStatement
     *
     * @param lNum linje nummer
     */
    public EmptyStatement(int lNum){
        super(lNum);
    }


    /**
     * function for java compiler
     * @param curScope scope
     * @param lib library
     */
    public void check(Block curScope, Library lib){}

    /**
     * EMPTY
     * @param f
     */
    @Override
    public void genCode(CodeFile f, Block b) {

    }

    /**
     * identify metode for EmptyStatement
     *
     * @return returnere identify String
     */
    @Override
    public String identify(){
        return "<empty statement> on line " + lineNum;
    }

    /**
     * prettyPrint metode for EmptyStatement
     */
    @Override
    public void prettyPrint(){
        Main.log.prettyPrint("; ");
    }

    /**
     * metoden parser en EmptyStatement
     *
     * @param s Scanneren
     * @return et EmptyStatement objekt
     */
    public static EmptyStatement parse(Scanner s) {
        enterParser("empty statement");
        EmptyStatement et = new EmptyStatement(s.curLineNum());
        s.skip(semicolonToken);
        leaveParser("empty statement");
        return et;
    }
}