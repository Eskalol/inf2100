package no.uio.ifi.pascal2100.parser;
import no.uio.ifi.pascal2100.scanner.Scanner;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.main.*;
import java.util.ArrayList;



/**
 * klasse for VarDeclPart
 *
 * @author Nicolay David Mohebi
 * @author Eskil Opdahl Nordland
 */
public class VarDeclPart extends PascalSyntax {
    ArrayList<VarDecl> varDecl;

    /**
     * Konstruktoer for VarDeclPart
     *
     * @param lNum linje nummer
     */
    public VarDeclPart(int lNum) {
        super(lNum);
        varDecl = new ArrayList<VarDecl>();
    }

    /**
     * check function for VarDeclPart
     * @param curScope scope
     * @param lib library
     */
    public void check(Block curScope, Library lib){
        for (VarDecl vd : varDecl)
            vd.check(curScope, lib);
    }

    /**
     * genCode for VardeclPart
     * @param f CodeFile
     * @param b Block
     */
    @Override
    void genCode(CodeFile f, Block b) {
        int nextOffset = b.offset + 4;
        for(VarDecl vd: varDecl) {
            System.out.println(nextOffset);
            vd.declLevel = b.getBlocklvl();
            vd.declOffset = nextOffset;
            nextOffset += 4;
        }
    }

    public int getDeclCnt() {
        return varDecl.size();
    }

    /**
     * identify metode for VarDeclPart
     *
     * @return returnere identify String
     */
    public String identify() {
        return "<var decl part> on line " + lineNum;
    }

    /**
     * prettyPrint metode for VarDeclPart
     */
    public void prettyPrint() {
        Main.log.prettyPrint("var ");
        for(VarDecl vd: varDecl)
            vd.prettyPrint();
    }

    /**
     * metoden parser en VarDeclPart
     *
     * @param s Scanneren
     * @return et VarDeclPart objekt
     */
    public static VarDeclPart parse(Scanner s) {
        enterParser("var decl part");
        VarDeclPart vdp = new VarDeclPart(s.curLineNum());
        vdp.varDecl = new ArrayList<VarDecl>();
        s.skip(varToken);
        while(s.curToken.kind == nameToken) vdp.varDecl.add(VarDecl.parse(s));

        leaveParser("var decl part");
        return vdp;
    }
}
