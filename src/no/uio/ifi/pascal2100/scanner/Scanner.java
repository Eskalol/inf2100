package no.uio.ifi.pascal2100.scanner;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

import java.io.*;
import java.lang.System;

public class Scanner {
	public Token curToken = null, nextToken = null;

	private LineNumberReader sourceFile = null;
	private String sourceFileName, sourceLine = "";
	private int sourcePos = 0;

	public Scanner(String fileName) {
		sourceFileName = fileName;
		try {
			sourceFile = new LineNumberReader(new FileReader(fileName));
		} catch (FileNotFoundException e) {
			Main.error("Cannot read " + fileName + "!");
		}
		readNextToken(); readNextToken();
	}

	public String identify() {
		return "Scanner reading " + sourceFileName;
	}


	public int curLineNum() {
		return curToken.lineNum;
	}


	private void error(String message) {
		Main.error("Scanner error on line " + curLineNum() + ": " + message);
	}

	/**
	 * funkjsonen tar en char som parameter og returnerer en int fra 1-4
	 * 1 = char er en bokstav
	 * 4 = char er et tall 0-9
	 * 3 = char en " eller '
	 * 2 = er ingen av de andre og er dermed et spesialtegn
	 * @param c
	 * @return
	 */
	private int checkChar(char c) {
		if(c >= '0' && c <= '9')
			return 4;
		else if(c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z')
			return 1;
		else if(c == '\"')
			return 3;
		else if(c == '\'')
			return 5;
		else
			return 2;
	}

	/**
	 * funksjonen sjekker char som er paa sourcePos er starten paa en kommentar.
	 * hvis det er er slik saa hopper den over til slutten av kommentaren
	 */
	private void Checkcomment() {
		if(sourceLine.charAt(sourcePos) == '{') {
			while(!sourceLine.contains("}") && sourceLine.length() != 0)
				readNextLine();
			if(sourceLine.length() == 0)
				Main.warning("scanner Warning: infinite comment");
			else {
				for (; sourceLine.charAt(sourcePos) != '}'; sourcePos++) ;
				sourcePos++;
			}
		}else if(sourceLine.length() > sourcePos + 1 && sourceLine.substring(sourcePos, sourcePos + 2).equals("/*")){
			while(!sourceLine.contains("*/") && sourceLine.length() != 0) //finner linjen som inneholder */ eller eof
				readNextLine();
			if(sourceLine.length() == 0)
				Main.warning("scanner Warning: infinite comment");
			else {
				for (; !sourceLine.substring(sourcePos, sourcePos + 2).equals("*/"); sourcePos++) ;
				sourcePos += 2;
			}
		}
		skipWhitespace();
	}

	/**
	 * funksjone sjekker om det er EOL
	 * hvs det er end of line saa leser den inn en ny linje
	 * og hvis linjen inneholder noe saa leser den inn en ny token
	 * @return
	 */
	private boolean checkEOL() {
		if(sourceLine.length() <= sourcePos) {
			readNextLine();
			if (sourceLine.length() != 0)
				readNextToken();
			return true;
		}
		return false;
	}

	/**
	 * funksjonen hopper over whitespace
	 */
	private void skipWhitespace() {
		while(sourceLine.length() > sourcePos && (sourceLine.charAt(sourcePos) == ' ' || sourceLine.charAt(sourcePos) == '\t'))
			sourcePos++;
	}

	/**
	 * funksjonen leser inn en ny token og vil alltid lese inn en token hver gang den blir kalt paa
	 * med mindre det er EOF
	 */
	public void readNextToken() {
		// Del 1 her
		String s = "";
		if(sourceLine.length() > 0 && sourceLine.length() > sourcePos) {
			//hopper over whitespace
			skipWhitespace();
			if(checkEOL())
				return;
			//sjekker kommentar
			Checkcomment();
			if(checkEOL())
				return;

			if(checkChar(sourceLine.charAt(sourcePos)) == 2) {
				//special character
				s += sourceLine.charAt(sourcePos++);
				if (sourceLine.charAt(sourcePos) == '=' || sourceLine.charAt(sourcePos) == '>' || sourceLine.charAt(sourcePos) == '.')
					s += sourceLine.charAt(sourcePos++);
				TokenKind tk = null;

				switch (s){
					case "+": tk = addToken; break;
					case ":=": tk = assignToken; break;
					case ":": tk = colonToken; break;
					case ",": tk = commaToken; break;
					case ".": tk = dotToken; break;
					case "=": tk = equalToken; break;
					case ">": tk = greaterToken; break;
					case ">=": tk = greaterEqualToken; break;
					case "[": tk = leftBracketToken; break;
					case "(": tk = leftParToken; break;
					case "<": tk = lessToken; break;
					case "<=": tk = lessEqualToken; break;
					case "*": tk = multiplyToken; break;
					case "<>": tk = notEqualToken; break;
					case "..": tk = rangeToken; break;
					case "]": tk = rightBracketToken; break;
					case ")": tk = rightParToken; break;
					case ";": tk = semicolonToken; break;
					case "-": tk = subtractToken; break;
					default: tk = null;
				}
				if(tk == null)
					Main.error("scanner Error: " + s + " not valid");

				curToken = nextToken;
				nextToken = new Token(tk, getFileLineNum());
			}
			else if(checkChar(sourceLine.charAt(sourcePos)) == 3) {
				//string value
				while(sourceLine.length() > sourcePos + 1 && checkChar(sourceLine.charAt(++sourcePos)) != 3)
					s += sourceLine.charAt(sourcePos);
				sourcePos++;
				curToken = nextToken;
				nextToken = new Token(s, s, getFileLineNum());
			}
			else if(checkChar(sourceLine.charAt(sourcePos)) == 5) {
				//string value
				while(sourceLine.length() > sourcePos + 1 && checkChar(sourceLine.charAt(++sourcePos)) != 5) {
					s += sourceLine.charAt(sourcePos);
					//sjekker for fnutt
					if(sourceLine.length() > sourcePos +3 && sourceLine.substring(sourcePos + 1, sourcePos + 3).equals("\'\'")) {
						s += "\'";
						sourcePos += 2;
					}
				}
				sourcePos++;
				curToken = nextToken;
				nextToken = new Token(s, s, getFileLineNum());
			}
			else if(checkChar(sourceLine.charAt(sourcePos)) == 4) {
				//int value
				while(checkChar(sourceLine.charAt(sourcePos)) == 4)
					s += sourceLine.charAt(sourcePos++);
				curToken = nextToken;
				nextToken = new Token(Integer.parseInt(s), getFileLineNum());
			}
			else {
				while (checkChar(sourceLine.charAt(sourcePos)) == 1 || checkChar(sourceLine.charAt(sourcePos)) == 4)
					s += sourceLine.charAt(sourcePos++);
				curToken = nextToken;
				nextToken = new Token(s, getFileLineNum());
			}
			Main.log.noteToken(nextToken);
		} else {
			readNextLine();
			if (sourceLine.length() > 0)
				readNextToken();
			else {
				curToken = nextToken;
				nextToken = new Token(eofToken, getFileLineNum());
				Main.log.noteToken(nextToken);
			}
		}
	}

	private void readNextLine() {
		if (sourceFile != null) {
			try {
				sourceLine = sourceFile.readLine();
				if (sourceLine == null) {
					sourceFile.close();  sourceFile = null;
					sourceLine = "";
					//modda litt
					curToken = nextToken;
					nextToken = new Token(eofToken, getFileLineNum());
					Main.log.noteToken(nextToken);
				} else {
					sourceLine += " ";
				}
				sourcePos = 0;
			} catch (IOException e) {
				Main.error("Scanner error: unspecified I/O error!");
			}
		}
		if (sourceFile != null)
			Main.log.noteSourceLine(getFileLineNum(), sourceLine);
	}


	private int getFileLineNum() {
		return (sourceFile!=null ? sourceFile.getLineNumber() : 0);
	}


	// Character test utilities:

	private boolean isLetterAZ(char c) {
		return 'A'<=c && c<='Z' || 'a'<=c && c<='z';
	}


	private boolean isDigit(char c) {
		return '0'<=c && c<='9';
	}


	// Parser tests:

	public void test(TokenKind t) {
		if (curToken.kind != t)
			testError(t.toString());
	}

	public void testError(String message) {
		Main.error(curLineNum(),
				"Expected a " + message +
						" but found a " + curToken.kind + "!");
	}

	public void skip(TokenKind t) {
		test(t);
		readNextToken();
	}
}
